var assert = require('assert');
var Vue = require('vue');
var ProgressBar = require('../app/js/module/ProgressBar');

describe('ProgressBar Object', function(){
  var progressBar = new ProgressBar(130,90);
  it('object created successfully', function(){
    assert.equal('object', typeof progressBar);
  })
  it('progressbar limit is 130', function(){
    assert.equal(130, progressBar.limit);
  })
  it('progressbar value is 90', function(){
    assert.equal(90, progressBar.value);
  })
  it('percent value is 69%', function(){
    assert.equal(69, progressBar.percent);
  })

  it('add value 25, value should now be 115', function(){
    progressBar.addValue(25);
    assert.equal(115, progressBar.value);
  });
  it('percent value should be 88%', function(){
    assert.equal(88, progressBar.percent);
  });

  it('add value 15, value should now be 130', function(){
    progressBar.addValue(15);
    assert.equal(130, progressBar.value);
  });
  it('percent value should be 100%', function(){
    assert.equal(100, progressBar.percent);
  });

  it('add value 30, value should now be 160', function(){
    progressBar.addValue(30);
    assert.equal(160, progressBar.value);
  });
  it('percent value should be 100%', function(){
    assert.equal(100, progressBar.percent);
  });

  it('percent label should be 123% (displayed in progress bar)', function(){
    assert.equal(123, progressBar.percentLabel);
  });
  it('minus value 125, value should now be 15', function(){
    progressBar.addValue(-125);
    assert.equal(35, progressBar.value);
  });

  it('percent value should be 26%', function(){
    assert.equal(26, progressBar.percent);
  });
  it('minus value 50, value cannot be lower than 0, value should now be 0', function(){
    progressBar.addValue(-50);
    assert.equal(0, progressBar.value);
  });
  it('percent value should be 0%', function(){
    assert.equal(0, progressBar.percent);
  });
});


