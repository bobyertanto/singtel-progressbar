
class ProgressBar {
  constructor(limit, defaultValue) {
    this.limit = limit;
    this.percent = 0;
    this.percentLabel = 0;
    this.setValue( defaultValue );
  }

  setValue(value){
    this.value = value;
    if (this.value < 0) this.value = 0;
    let percent = Math.floor(this.value / this.limit * 100);
    this.percent = percent > 100 ? 100 : percent;
    this.percentLabel = percent;
  }
  addValue(value){
    this.value += value;
    this.setValue(this.value);
  }
}

module.exports = ProgressBar;
