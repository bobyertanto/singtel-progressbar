import Vue from 'vue';
import axios from 'axios';
import ProgressBar from './module/ProgressBar';

var app = new Vue({
  el: '.progressbar.component',
  data: {
    selectedProgressBar: 0,
    buttons: [],
    progressbar: [],
    isLoaded: false
  },
  mounted: () => {
    axios
    .get('https://pb-api.herokuapp.com/bars')
    .then( (result) => {
      app.buttons = result.data.buttons;
      result.data.bars.forEach((value) => {
        let progressBar = new ProgressBar(result.data.limit, value);
        app.progressbar.push(progressBar)
      });
      app.isLoaded = true;
    })
  },
  methods:{
    updateProgressBar: (addValue) => {
      let progress = app.progressbar[app.selectedProgressBar]
      progress.addValue(addValue);
    }
  }
})
